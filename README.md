#docker academy installation guide

This is the setup guid to prepare for the docker academy course. 
Please install and execute all steps as described below unless you are running on a 
linux operating system. In this case please see under Linux OS at the bottom of this page.


##Virtualbox

Docker is running currently only under linux. Windows or Mac users need to spin up a 
virtual machine running linux to use docker. In this course we will use 
Virtualbox >= 5.0.10

* Download and install virtualbox from [here](https://www.virtualbox.org/wiki/Downloads) on your local host

##Vagrant

To ease the configuration and deployment of the virtual machine we use vagrant.
 
* Download and install vagrant. IMPORTATNT - Vagrant version *1.8.6* introduced a bug which prevents `vagrant up` from completing. Therefore you need to avoid this version and download 1.8.5 from [here](https://releases.hashicorp.com/vagrant/1.8.5/).

##git
Documents and artifact are version controlled with git.

* Download and install git from [here](https://git-scm.com/downloads) on your local host

##Initialize academy-environment
Now it is time to initialize the docker-acacademy environment.

* Clone the docker-acacademy-public repository to a directory of your choice on your local host. (Make sure you have at least 10G free space on the drive)
```
git clone https://bitbucket.org/ti8m/docker-academy-public.git
```

* Navigate on the command line into the directory docker-academy-public

* Execute the following command, it will download and start the vagrant box [ti8m/docker-academy](https://atlas.hashicorp.com/ti8m/boxes/docker-academy) (If you already installed the docker-academy box before you can upgrade it with vagrant box update, should be 1.3)
```
vagrant up
```
* If everything went fine, you should be able to connect to the running vagrant box
```
vagrant ssh
```
* Change to the root user (there is also a user academy which can run docker)
```
sudo su -
```
* Move into the shared vagrant directory (this is the same directory as you checked out on your host)
```
cd /vagrant
```
*In case this directory does not exist you most likely have an issue with vbguest additions.*

* Excute the installation check script
On Windows
```
./installation_check.sh
```
On Linux and Mac
```
chmod +x ./installation_check.sh
./installation_check.sh
```

###Problems
If you experience any problems while executing the steps above, please drop an email to <trt@ti8m.ch> and <anf@ti8m.ch> where you describe the problem and if possible a copy of the error message. We will then contact you as soon as possible

##Optional
No required but can be quite helpful to have the following tools available.

* ssh-client like [putty](http://www.putty.org/)
* editor of your choice like [np++](https://notepad-plus-plus.org/) or [sublime](https://www.sublimetext.com/)


##Linux OS
If you run native linux, please install the following

* docker >= 1.12
* docker-compose >= 1.8